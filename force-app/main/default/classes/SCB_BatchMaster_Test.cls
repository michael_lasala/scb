@isTest
private class SCB_BatchMaster_Test {
    @TestSetup
    static void createData()
    {
        Account acc = new Account(
            Name = 'Test Account'
        );
        insert acc;
    }

    private static testMethod void batchMasterExtensionPositiveTestConfig1() 
    {
        Test.startTest();
        SCB_BatchMaster instance = new SCB_BatchMaster();
        instance.batchConfig = new Batch_Configuration__mdt(
            DeveloperName = 'PACS_TestBatch',
            MasterLabel = 'PACS_TestBatch',
            Batch_SOQL__c = 'SELECT Id FROM Account',
            Full_Load__c = false,
            Delta_Load_Condition__c = 'CreatedDate >= {startDate} AND CreatedDate <= {endDate}',
            Max_Reprocess_Count__c = 1,
            Reprocess__c = true,
            Start_Timestamp__c = DateTime.now().addDays(-1),
            End_Timestamp__c = DateTime.now().addDays(1),
            Batch_Result_Email_To_Address__c = 'test@test.com',
            Batch_Result_Email_Body_String__c = '{batchName},{recordCount},{errorCount},{successCount},{failedSetString},{processLog}',
            Batch_Result_Option__c = 'All'
        );
        instance.init('PACS_AgencyUserCreationSetupObject');
        instance.batchConfig.Next_Batch_Class__c = '';
        Database.executeBatch(instance);
        Test.stopTest();
    }

    private static testMethod void batchMasterExtensionPositiveTestConfig2() 
    {
        Test.startTest();
        SCB_BatchMaster instance = new SCB_BatchMaster();
        instance.batchConfig = new Batch_Configuration__mdt(
            DeveloperName = 'PACS_TestBatch',
            MasterLabel = 'PACS_TestBatch',
            Batch_SOQL__c = 'SELECT Id FROM Account WHERE Name !=null',
            Full_Load__c = false,
            Delta_Load_Condition__c = 'WHERE CreatedDate >= {startDate} AND CreatedDate <= {endDate}',
            Max_Reprocess_Count__c = 1,
            Reprocess__c = true,
            Start_Timestamp__c = DateTime.now().addDays(-1),
            End_Timestamp__c = DateTime.now().addDays(1),
            Batch_Result_Email_To_Address__c = 'test@test.com',
            Batch_Result_Email_Body_String__c = '{batchName},{recordCount},{errorCount},{successCount},{failedSetString},{processLog}',
            Batch_Result_Option__c = 'None',
            Next_Batch_Class__c = ''
        );
        instance.init('PACS_TestBatch');
        Database.executeBatch(instance);
        Test.stopTest();
    }

    // private static testMethod void batchMasterExtensionPositiveTestConfig3() 
    // {
    //     Test.startTest();
    //     SCB_BatchMaster instance = new SCB_BatchMaster();
    //     instance.configurationName = 'PACS_AgencyUserCreationSetupObject';
    //     instance.init(instance.configurationName);
    //     instance.batchConfig.Next_Batch_Class__c = '';
    //     instance.batchConfig.Full_Load__c= true;
    //     instance.batchConfig.Batch_SOQL__c += ' LIMIT 1';
    //     Database.executeBatch(instance);
    //     Test.stopTest();
    // }
    
    private static testMethod void batchMasterExtensionPositiveTestConfig4() 
    {
        Test.startTest();
        SCB_BatchMaster instance = new SCB_BatchMaster();
        instance.batchConfig = new Batch_Configuration__mdt(
            DeveloperName = 'PACS_AgencyUserCreation',
            MasterLabel = 'PACS_AgencyUserCreation',
            Batch_SOQL__c = 'SELECT Id FROM Account',
            Full_Load__c = false,
            Delta_Load_Condition__c = 'CreatedDate >= {startDate} AND CreatedDate <= {endDate}',
            Max_Reprocess_Count__c = 1,
            Reprocess__c = true,
            Start_Timestamp__c = DateTime.now().addDays(-1),
            End_Timestamp__c = DateTime.now().addDays(1),
            Batch_Result_Email_To_Address__c = 'test@test.com',
            Batch_Result_Email_Body_String__c = '{batchName},{recordCount},{errorCount},{successCount},{failedSetString},{processLog}',
            Batch_Result_Option__c = 'All',
            Batch_Size__c = 200
        );
        instance.init();
        instance.configurationName = 'PACS_AgencyUserCreationSetupObject';
        instance.JobName = 'PACS_AgencyUserCreationSetupObject';
        instance.batchConfig.Next_Batch_Class__c = '';
        for(Account acc : [SELECT Id FROM Account LIMIT 1]){
            instance.errorCount++;
        }
        Database.executeBatch(instance);
        instance.logFinishToEmail('TestBatch', 1, 1, 2, true);
        Test.stopTest();
    }

    private static testMethod void testInsertHandler(){
        Test.startTest();
            List<Account> accountForInsertList = SCB_TestDataFactory.createPersonAccounts(10);
            SCB_BatchMaster instance = new SCB_BatchMaster();
            instance.handlerDMLResult(Database.insert(accountForInsertList));
        Test.stopTest();
    }
    
    private static testMethod void testInsertHandlerNegative(){
        Test.startTest();
            List<Account> accountForInsertList = SCB_TestDataFactory.createPersonAccounts(10);
            SCB_BatchMaster instance = new SCB_BatchMaster();
                instance.handlerDMLResult(Database.insert(accountForInsertList,false));
            
        Test.stopTest();
    }

    private static testMethod void testUpsertHandler(){
        Test.startTest();
            Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            List<Account> accountForInsertList = SCB_TestDataFactory.createPersonAccounts(10);
            Account accNegative = new Account(
                Name = '',
                RecordTypeId = accountRecordTypeId
            );
            accountForInsertList.add(accNegative);
            SCB_BatchMaster instance = new SCB_BatchMaster();
                instance.handlerDMLResult(Database.upsert(accountForInsertList,false));
            
        Test.stopTest();
    }

    private static testMethod void testDeleteHandler(){
        Test.startTest();
            List<Database.DeleteResult> deleteResultMock = new List<Database.DeleteResult>();
            deleteResultMock.add((Database.DeleteResult)JSON.deserialize('{"success":true,"id":"0013000000abcde"}', Database.DeleteResult.class));
            deleteResultMock.add((Database.DeleteResult)JSON.deserialize('{"success":false,"errors":[{"message":"Failed Validation","statusCode":"FIELD_CUSTOM_VALIDATION_EXCEPTION"}]}', Database.DeleteResult.class));
            SCB_BatchMaster instance = new SCB_BatchMaster();
            instance.handlerDMLResult(deleteResultMock);
            
        Test.stopTest();
    }

    // private static testMethod void testInit(){
    //     Test.startTest();
    //     SCB_BatchMaster instance = new SCB_BatchMaster();
    //         instance.init('PACS_AgencyUserCreationSetupObject');
            
    //     Test.stopTest();
    // }
    
    private static testMethod void testMockNonDMLHandler(){
        Test.startTest();
            List<Account> accountForInsertList = SCB_TestDataFactory.createPersonAccounts(10);
            insert accountForInsertList;
            SCB_BatchMaster instance = new SCB_BatchMaster();
            System.assertEquals(0, instance.errorLogList.size());
            instance.handleNonDMLProcess('An error has occured', accountForInsertList.get(0).Id);
            instance.handleNonDMLProcess(new SCB_BatchMaster.BATCH_CLASS_EXCEPTION('Batch Class error occured'), accountForInsertList.get(0).Id);
            System.assertEquals(2, instance.errorLogList.size());
        Test.stopTest();
    }

    @isTest
    private static void getStringParam() {
        SCB_BatchMaster instance = new SCB_BatchMaster();
        instance.staticParameters = new Map<String,String> {
            'StringParam' => 'StringValue'
        };

        String val = instance.getStringParam('StringParam');
        System.assertEquals('StringValue', val);
    }

    @isTest
    private static void getBooleanParam() {
        SCB_BatchMaster instance = new SCB_BatchMaster();
        instance.staticParameters = new Map<String,String> {
            'BooleanParam' => 'true'
        };

        Boolean val = instance.getBooleanParam('BooleanParam');
        System.assertEquals(true, val);
    }

    @isTest
    private static void getIntegerParam() {
        SCB_BatchMaster instance = new SCB_BatchMaster();
        instance.staticParameters = new Map<String,String> {
            'IntegerParam' => '1'
        };

        Integer val = instance.getIntegerParam('IntegerParam');
        System.assertEquals(1, val);
    }
}