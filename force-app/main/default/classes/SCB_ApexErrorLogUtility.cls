public class SCB_ApexErrorLogUtility {

    public static void logError(String triggerName, 
                                String className, 
                                String methodName, 
                                DmlException exp){

        List<Apex_Error_Log__c> errorLogs = new List<Apex_Error_Log__c>();

        Apex_Error_Log__c log = new Apex_Error_Log__c(
            Trigger_Name__c = triggerName,
            Class_Name__c = className,
            Method_Name__c = methodName
        );

        if(exp != null) {
            log.Exception_Type__c = exp.getTypeName();
            log.Exception_Message__c = exp.getMessage() + + '\r\n' + exp.getStackTraceString();
            log.Line_Number__c = String.valueOf(exp.getLineNumber());
            log.Record_Id__c = exp.getDmlId(0);
        }
        errorLogs.add(log);

        logErrorList(errorLogs);
    }

    public static Apex_Error_Log__c logError(String triggerName, 
                                             String className, 
                                             String methodName, 
                                             Id recordId, 
                                             String errorMessage){

        Apex_Error_Log__c log = new Apex_Error_Log__c(
            Trigger_Name__c = triggerName,
            Class_Name__c = className,
            Method_Name__c = methodName, 
            Exception_Message__c = errorMessage, 
            Record_Id__c = recordId
        );
        return log;
    }

    public static Apex_Error_Log__c createBatchLogInstance(String triggerName, 
                                                           String className, 
                                                           String methodName, 
                                                           String recordId, 
                                                           String typeName, 
                                                           String message, 
                                                           String lineNumber){
        Apex_Error_Log__c log = new Apex_Error_Log__c(
            Trigger_Name__c = triggerName,
            Class_Name__c = className,
            Method_Name__c = methodName, 
            Exception_Message__c = message, 
            Record_Id__c = recordId, 
            Exception_Type__c = typeName,
            Line_Number__c = lineNumber

        );
        return log;
    }

    public static void logErrorList(List<Apex_Error_Log__c> errorLogs){
        if(errorLogs == null || errorLogs.isEmpty()) return;
        
        if(!errorLogs.isEmpty()){
            try {
                insert errorLogs;
            } catch(Exception ex) {
                system.debug('Error:' + ex.getMessage());
            }
        }
    }

    public static void logDMLResult(String triggerName, 
                                    String className, 
                                    String methodName, 
                                    List<Database.SaveResult> srList){

        if(srList == null) return;

        List<Apex_Error_Log__c> errorLogs = new List<Apex_Error_Log__c>();   
        
        for(Database.SaveResult sr : srList){
            if(!sr.isSuccess()){
                String recordId = sr.getId();
                String exceptionMessage = '';
                
                for(Database.Error err : sr.getErrors()){
                    exceptionMessage += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                    exceptionMessage += 'Fields Affected: ' + err.getFields() + '\r\n';
                }
                errorLogs.add(logError(triggerName, className, methodName, recordId, exceptionMessage));
            }
        }
        logErrorList(errorLogs);
    }

    public static void logDMLResult(String triggerName, 
                                    String className, 
                                    String methodName, 
                                    List<Database.DeleteResult> srList){

        if(srList == null) return;

        List<Apex_Error_Log__c> errorLogs = new List<Apex_Error_Log__c>(); 
        
        for(Database.DeleteResult sr : srList){
            if(!sr.isSuccess()){
                String recordId = sr.getId();
                String exceptionMessage = '';
                
                for(Database.Error err : sr.getErrors()){
                    exceptionMessage += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                    exceptionMessage += 'Fields Affected: ' + err.getFields() + '\r\n';
                }
                
                errorLogs.add(logError(triggerName, className, methodName, recordId, exceptionMessage));
            }
        }
        logErrorList(errorLogs);
    }
}