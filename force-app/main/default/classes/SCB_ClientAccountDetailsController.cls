public  class SCB_ClientAccountDetailsController {
    
    @AuraEnabled(cacheable=true)
    public static Map<String, Map<Decimal, List<DataSet>>> getSegments(Id accountId){

        Map<String, Set<String>> accountSegmentMap = SCB_ClientAumMixController.getAccountSegment();

        Map<String, Map<Decimal, List<DataSet>>> segmentMap = new Map<String, Map<Decimal, List<DataSet>>>();
        Map<String, Set<String>> accountTypeMap = new Map<String, Set<String>>();

        Account client = SCB_QueryWithoutSharing.getClientDetails(accountId);
        Map<Decimal, List<DataSet>> tempMap = new Map<Decimal, List<DataSet>>();
        List<DataSet> tempList = new List<DataSet>();
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_INVESTMENT, 
                                 SCB_Constants.ACCOUNT_TYPE_INVESTMENT, 
                                 client.FinServ__TotalInvestmentsPrimaryOwner__c, 
                                 null));
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_PCI, 
                                 SCB_Constants.ACCOUNT_TYPE_PCI, 
                                 client.Total_PCI_Primary_Owner__c, 
                                 null));
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_STRUCTURED_DEPOSIT, 
                                 SCB_Constants.ACCOUNT_TYPE_STRUCTURED_DEPOSIT, 
                                 client.Total_Structured_Deposit_Primary_Owner__c, 
                                 null));
        tempMap.put(client.Total_Wealth_Management_Primary_Owner__c, tempList);
        segmentMap.put(SCB_Constants.SEGMENT_TYPE_WEALTH_MANAGEMENT, tempMap);

        tempMap = new Map<Decimal, List<DataSet>>();
        tempList = new List<DataSet>();
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_CURRENT_ACCOUNT, 
                                 SCB_Constants.ACCOUNT_TYPE_CURRENT_ACCOUNT, 
                                 client.Total_Current_Account_Primary_Owner__c, 
                                 null));
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_SAVINGS_ACCOUNT, 
                                 SCB_Constants.ACCOUNT_TYPE_SAVINGS_ACCOUNT, 
                                 client.Total_Savings_Account_Primary_Owner__c, 
                                 null));
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_TIME_DEPOSIT, 
                                 SCB_Constants.ACCOUNT_TYPE_TIME_DEPOSIT, 
                                 client.Total_Time_Deposit_Primary_Owner__c, 
                                 null));
        tempMap.put(client.Total_Deposits_Primary_Owner__c, tempList);
        segmentMap.put(SCB_Constants.SEGMENT_TYPE_DEPOSITS, tempMap);

        tempMap = new Map<Decimal, List<DataSet>>();
        tempList = new List<DataSet>();
        tempMap.put(client.Total_Banca_Primary_Owner__c != null ? client.Total_Banca_Primary_Owner__c : 0, tempList);
        segmentMap.put(SCB_Constants.SEGMENT_TYPE_BANCA, tempMap);

        tempMap = new Map<Decimal, List<DataSet>>();
        tempList = new List<DataSet>();
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_OVERDRAFT, 
                                 SCB_Constants.ACCOUNT_TYPE_OVERDRAFT, 
                                 client.Total_Overdraft_Primary_Owner__c, 
                                 null));
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_MORTGAGE, 
                                 SCB_Constants.ACCOUNT_TYPE_MORTGAGE, 
                                 client.Total_Mortgage_Primary_Owner__c, 
                                 null));
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_PERSONAL_LOAN, 
                                 SCB_Constants.ACCOUNT_TYPE_PERSONAL_LOAN, 
                                 client.Total_Personal_Loan_Primary_Owner__c, 
                                 null));
        tempList.add(new DataSet(SCB_Constants.ACCOUNT_TYPE_CREDIT_CARD, 
                                 SCB_Constants.ACCOUNT_TYPE_CREDIT_CARD, 
                                 client.Total_Credit_Card_Primary_Owner__c, 
                                 null));
        tempMap.put(client.Total_Loans_Primary_Owner__c, tempList);
        segmentMap.put(SCB_Constants.SEGMENT_TYPE_LOANS, tempMap);
        return segmentMap;
    }

    @AuraEnabled(cacheable=true)
    public static Map<String, List<DataSet>> getFinancialAccounts(Id accountId, String recordTypeName){
        Map<String, String> recordTypeNameMap = new Map<String, String>();
        for(Account_Product__mdt ap: SCB_QueryWithoutSharing.getAccountProduct()){
            recordTypeNameMap.put(ap.MasterLabel, ap.Financial_Account_Record_Type_API_Name__c);
        }

        Map<String, List<DataSet>> financialAccountMap = new Map<String, List<DataSet>>();

        for(FinServ__FinancialAccount__c financialAccount: SCB_QueryWithoutSharing.getFinancialAccounts(accountId, recordTypeNameMap.get(recordTypeName))){
            if(!financialAccountMap.containsKey(recordTypeName)){
                List<DataSet> tempList = new List<DataSet>();
                tempList.add(new DataSet(financialAccount.Id, 
                                         financialAccount.Name, 
                                         financialAccount.FinServ__Balance__c, 
                                         financialAccount.FinServ__LoanEndDate__c));
                
                financialAccountMap.put(recordTypeName, tempList);
            } else {
                financialAccountMap.get(recordTypeName).add(new DataSet(financialAccount.Id, 
                                                                        financialAccount.Name, 
                                                                        financialAccount.FinServ__Balance__c, 
                                                                        financialAccount.FinServ__LoanEndDate__c));
            }
        }
        return financialAccountMap;
    }

    public class DataSet {
        public DataSet(String recordId, String accountType, Decimal balance, Date maturityDate){
            this.recordId = recordId;
            this.accountType = accountType;
            this.balance = balance;
            this.maturityDate = maturityDate;
        }

        @AuraEnabled
        public String recordId {get;set;}
        @AuraEnabled
        public String accountType {get;set;}
        @AuraEnabled
        public Decimal balance {get;set;}
        @AuraEnabled
        public Date maturityDate {get;set;}
    }
}