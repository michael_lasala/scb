@isTest
private class SCB_ApexErrorLogUtility_Test {
    @testSetup static void staticRecords(){
        
    }
    
    @isTest static void exception_Test() {
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(10);
        for(Account acc: accountList){
            acc.LastName = null;
        }

        try{ 
            insert accountList;
        } catch (DmlException ex){
            SCB_ApexErrorLogUtility.logError('Trigger', 'Class', 'Method', ex);
        }

        for(Apex_Error_Log__c log: [SELECT Trigger_Name__c, Class_Name__c, Method_Name__c, Exception_Message__c, Exception_Type__c FROM Apex_Error_Log__c]){
            System.assertEquals('Trigger', log.Trigger_Name__c);
            System.assertEquals('Class', log.Class_Name__c);
            System.assertEquals('Method', log.Method_Name__c);
            System.assert(log.Exception_Type__c.contains('DmlException'));
            System.assert(log.Exception_Message__c.contains('REQUIRED_FIELD_MISSING'));
        }
    }

    @isTest static void insert_Test() {
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(10);
        for(Account acc: accountList){
            acc.LastName = null;
        }

        List<Database.SaveResult> srList = Database.insert(accountList, false);
        SCB_ApexErrorLogUtility.logDMLResult('Trigger', 'Class', 'Method', srList);

        for(Apex_Error_Log__c log: [SELECT Trigger_Name__c, Class_Name__c, Method_Name__c, Exception_Message__c FROM Apex_Error_Log__c]){
            System.assertEquals('Trigger', log.Trigger_Name__c);
            System.assertEquals('Class', log.Class_Name__c);
            System.assertEquals('Method', log.Method_Name__c);
            System.assert(log.Exception_Message__c.contains('REQUIRED_FIELD_MISSING'));
        }
    }

    @isTest static void delete_Test() {
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(10);
        insert accountList;

        delete accountList;

        List<Database.DeleteResult> srList = Database.delete(accountList, false);
        SCB_ApexErrorLogUtility.logDMLResult('Trigger', 'Class', 'Method', srList);

        for(Apex_Error_Log__c log: [SELECT Trigger_Name__c, Class_Name__c, Method_Name__c, Exception_Message__c FROM Apex_Error_Log__c]){
            System.assertEquals('Trigger', log.Trigger_Name__c);
            System.assertEquals('Class', log.Class_Name__c);
            System.assertEquals('Method', log.Method_Name__c);
            System.assert(log.Exception_Message__c.contains('ENTITY_IS_DELETED'));
        }
    }
}