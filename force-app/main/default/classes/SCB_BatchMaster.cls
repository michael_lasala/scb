global virtual class SCB_BatchMaster implements Database.Batchable<SObject>, Database.Stateful {
    global Batch_Configuration__mdt batchConfig;
    global Integer recordCount = 0;
    global Integer successCount = 0;
    global Integer errorCount = 0;
    global Integer reprocessCount = 0;
    global Set<Id> failedIdSet = new Set<Id>();
    global DateTime startDate = DateTime.now();
    global String configurationName;
    global String processLog = '';
    global String JobName = '';
    global Set <Id> reprocessIdSet;
    global Datetime startTime = DateTime.now();
    global Map<String,Object> inputs = new Map<String,Object>();
    global Map<String,String> staticParameters = new Map<String,String>();
    global Boolean batchResultOption = true; //DEFAULT BATCH OPTION TO ALL

    //EXCEPTION HANDLERS
    global List<Apex_Error_Log__c> errorLogList = new List<Apex_Error_Log__c>();
    global class BATCH_CLASS_EXCEPTION extends Exception{}
    global class BATCH_CONFIGURATION_EXCEPTION extends Exception{}
    global class BATCH_STATIC_PARAM_KEY_NOT_FOUND_EXCEPTION extends Exception{}
    global final String BATCH_CONFIG_NOT_FOUND = 'Batch Process Configuration Not Found. Please Contact System Administrator.';
    global final String BATCH_CLASS_NOT_FOUND = 'Batch Process Not Found. Please Contact System Administrator.';

    //Constructor
    global SCB_BatchMaster(){
        failedIdSet = new Set<Id>();
    }

    //Initialization method that will fetch Batch Configuration
    global void init(){
        String className = getBatchClassName();
        init(className, className);
    }

    //Initialization method that will fetch Batch Configuration
    global void init(String configurationName){
        String className = getBatchClassName();
        init(configurationName, className);
    }

    //Initialization method that will fetch Batch Configuration
    global void init(String configurationName, String jobName){
        this.configurationName = configurationName;
        this.JobName = jobName;

        //Retrieves Batch Configuration custom metadata of the batch job
        batchConfig = getSetting(this.configurationName);
        if(batchConfig == null){
            return;
        }
        if(batchConfig.Batch_Result_Option__c == 'All'){
            batchResultOption = true;
        } else if(batchConfig.Batch_Result_Option__c == 'None'){
            batchResultOption = false;
        }

        //Commented out for now. No usage at this point in the project
        // if(batchConfig.Batch_Static_Parameters__r != null && !batchConfig.Batch_Static_Parameters__r.isEmpty()) {
        //     for(Batch_Static_Parameter__mdt param : batchConfig.Batch_Static_Parameters__r) {
        //         staticParameters.put(param.DeveloperName, param.Value__c);
        //     }
        //     System.debug('DBG - Batch_Static_Parameters: ' + JSON.serializePretty(staticParameters));
        // }
    }

    //Gets String value from Batch Static Parameter map
    global String getStringParam(String paramName){
        if(staticParameters.containsKey(paramName)){
            return staticParameters.get(paramName);
        }
        throw new BATCH_STATIC_PARAM_KEY_NOT_FOUND_EXCEPTION('Input static parameter key: ' + paramName + ' not found');
    }

    //Gets Boolean value from Batch Static Parameter map
    global Boolean getBooleanParam(String paramName){
        String val = getStringParam(paramName);
        return Boolean.valueOf(val);
    }

    //Gets Integer value from Batch Static Parameter map
    global Integer getIntegerParam(String paramName){
        String val = getStringParam(paramName);
        return Integer.valueOf(val);
    }

    //Virtual Batch Apex start Method
    global virtual Database.QueryLocator start(Database.BatchableContext context){
        if(batchConfig != null && String.isNotBlank(batchConfig.Batch_SOQL__c)){
            String soqlString = batchConfig.Batch_SOQL__c;
            DateTime startDate = batchConfig.Start_Timestamp__c;
            DateTime endDate = batchConfig.End_Timestamp__c;
            
            if(!batchConfig.Full_Load__c){
                //Add condition to SOQL filter
                soqlString = addDeltaCondition(soqlString, replaceDeltaConditionMergeFields(batchConfig.Delta_Load_Condition__c));
            }

            if(reprocessIdSet!= null){
                //Adds failed record ids to SOQL filter for reprocessing
                if(reprocessIdSet.size() > 0){
                    soqlString = addDeltaCondition(soqlString, 'Id IN : reprocessIdSet');
                }
            }

            soqlString = preQuery(soqlString);
            return Database.getQueryLocator(soqlString);
        } else {
            throw new BATCH_CONFIGURATION_EXCEPTION(BATCH_CONFIG_NOT_FOUND);
        }
    }
    
    //Enables update of SOQL outside the batch configration
    global virtual String preQuery(String soqlString){
        return soqlString;
    }

    //Virtual Batch Apex execute Method
    global virtual void execute(Database.BatchableContext context, List<sObject> ldList){
        //Do nothing, by right, each batch class has a unique execute logic
    }

    //Virtual Batch Apex finish Method
    global virtual void finish(Database.BatchableContext bc){
        if(batchConfig.Reprocess__c && (batchConfig.Max_Reprocess_Count__c != null && batchConfig.Max_Reprocess_Count__c > 0) && failedIdSet.size() > 0){
            //Reprocess failed records if present
            if(failedIdSet.size() > 0){
                reprocessFailedRecords();
            }
           
            //Validates if reprocess occurrency has reached the max specified in the batch configuration
            if(reprocessCount == batchConfig.Max_Reprocess_Count__c){
                //Calls next batch class if present
                if(String.isNotBlank(batchConfig.Next_Batch_Class__c)){
                    callNextBatch(batchConfig.Next_Batch_Class__c);
                }
            }
        } else {
            //Calls next batch class if present
            if(String.isNotBlank(batchConfig.Next_Batch_Class__c)){
                callNextBatch(batchConfig.Next_Batch_Class__c);
            }
            handleAfterStopReprocessing();

            //Sends summary email based on batch configuration
            logFinishToEmail(bc.getJobId(), false); 
           
        }
        
        postFinish(bc);

        //Created Apex Error Log record which contains the summary of the batch job
        logFinishToObject(bc, true);
    }

    global virtual void postFinish(Database.BatchableContext bc) {
        //Do nothing, batch class can override this method if unique logic is applicable
    }

    //Virtual Post Process Method for DML Handling
    global virtual List<Database.SaveResult> handlerDMLPostProcess(List<Database.SaveResult> saveResultList){
        return saveResultList;
    }

    //Virtual Post Process Method for DML Handling
    global virtual List<Database.UpsertResult> handlerDMLPostProcess(List<Database.UpsertResult> upsertResultList){
        return upsertResultList;
    }

    //Virtual Post Process Method for DML Handling
    global virtual List<Database.DeleteResult> handlerDMLPostProcess(List<Database.DeleteResult> deleteResultList){
        return deleteResultList;
    }    

    //Method returning Batch Configuration
    global Batch_Configuration__mdt getSetting(String configName){
        if(batchConfig != null){
            return batchConfig;
        } else {
            return [SELECT
                        DeveloperName, Batch_Result_Email_Body_String__c, Batch_Result_Email_To_Address__c,
                        Batch_Result_Option__c, Batch_Size__c, Batch_SOQL__c, Delta_Load_Condition__c,
                        End_Timestamp__c, Full_Load__c, Max_Reprocess_Count__c, Next_Batch_Class__c,
                        Send_Email_To_Running_User__c, Reprocess__c, Start_Timestamp__c 
                        //Commented out for now. No usage at this point in the project
                        // (   
                        //     SELECT DeveloperName, Value__c 
                        //     FROM Batch_Static_Parameters__r 
                        // )    
                    FROM 
                        Batch_Configuration__mdt
                    WHERE
                        DeveloperName = : configName
                    LIMIT 1];
        }
    }

    //DML Handling Method. Includes Apex Error Logging.
    global void handlerDMLResult(List<Database.SaveResult> srList){
        //ADD RECORD COUNT
        addLogCount(0, 0, srList.size());
        for(Database.SaveResult sr : srList){
            if (sr.isSuccess()){
                //ADD SUCCESS COUNT
                addLogCount(1, 0, 0);
            } else {
                // Operation failed, so get all errors    
                //ADD ERROR COUNT
                addLogCount(0, 1, 0);
                failedIdSet.add(sr.getId());
                String exceptionMessage =  'The following error has occurred.['+ sr.getId() +']\r\n';
                for(Database.Error err : sr.getErrors()){
                    exceptionMessage += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                    exceptionMessage += 'Fields Affected: ' + err.getFields() + '\r\n';
                }
                //Created Apex Error Log record
                createErrorLogInstance(sr.getId(), 'Batch Failed Record', exceptionMessage, 'handlerDMLResult : Database.SaveResult');
            }
        }
        handlerDMLPostProcess(srList); 
    }

    //DML Handling Method. Includes Apex Error Logging.
    global void handlerDMLResult(List<Database.UpsertResult> srList){
        //ADD RECORD COUNT
        addLogCount(0, 0, srList.size());
        for(Database.UpsertResult sr : srList){
            if (sr.isSuccess()){
                //ADD SUCCESS COUNT
                addLogCount(1, 0, 0);
            } else {
                // Operation failed, so get all errors    
                //ADD ERROR COUNT
                addLogCount(0, 1, 0);
                failedIdSet.add(sr.getId());
                String exceptionMessage =  'The following error has occurred.['+ sr.getId() +']\r\n';
                for(Database.Error err : sr.getErrors()){
                    exceptionMessage += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                    exceptionMessage += 'Fields Affected: ' + err.getFields() + '\r\n';
                }
                //Created Apex Error Log record
                createErrorLogInstance(sr.getId(), 'Batch Failed Record', exceptionMessage, 'handlerDMLResult : Database.UpsertResult');
            }
       }
       handlerDMLPostProcess(srList);
    }

    //DML Handling Method. Includes Apex Error Logging.
    global void handlerDMLResult(List<Database.DeleteResult> srList){
        //ADD RECORD COUNT
        addLogCount(0, 0, srList.size());
        for(Database.DeleteResult sr : srList){
            if (sr.isSuccess()){
                //ADD SUCCESS COUNT
                addLogCount(1, 0, 0);
            } else {
                // Operation failed, so get all errors    
                //ADD ERROR COUNT
                addLogCount(0, 1, 0);
                failedIdSet.add(sr.getId());
                String exceptionMessage =  'The following error has occurred.['+ sr.getId() +']\r\n';
                for(Database.Error err : sr.getErrors()){
                    exceptionMessage += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                    exceptionMessage += 'Fields Affected: ' + err.getFields() + '\r\n';
                }
                //Created Apex Error Log record
                createErrorLogInstance(sr.getId(), 'Batch Failed Record', exceptionMessage, 'handlerDMLResult : Database.DeleteResult');
            }
       }
        handlerDMLPostProcess(srList);        
    }

    //Non DML Handling Method. Includes Apex Error Logging.
    global void handleNonDMLProcess(Exception e,Id recordId){
        addLogCount(0, 0, 1);
        if(e == null){
            addLogCount(1, 0, 0);
        } else {
            addLogCount(0, 1, 0);
            failedIdSet.add(recordId);
            String exceptionMessage;
            exceptionMessage = 'The following error has occurred.['+ recordId +']\r\n';
            exceptionMessage += 'Root Cause: '+ String.valueOf(e.getLineNumber()) + ' : ' +  e.getMessage() + '\r\n';
            //Created Apex Error Log record
            createErrorLogInstance(recordId, 'Batch Failed Record', exceptionMessage, '');
        }
    }

    //Non DML Handling Method. Includes Apex Error Logging.
    global void handleNonDMLProcess(String message,Id recordId){
        addLogCount(0, 0, 1);
        if(String.isBlank(message)){
            addLogCount(1, 0, 0);
        } else {
            addLogCount(0, 1, 0);
            if(String.isNotBlank(recordId)){
                failedIdSet.add(recordId);
            }
            String exceptionMessage;
            exceptionMessage = 'The following error has occurred.['+ recordId +']\r\n';
            exceptionMessage += 'Root Cause: '+ message + '\r\n';
            //Created Apex Error Log record
            createErrorLogInstance(recordId, 'Batch Failed Record', exceptionMessage, '');
        }
    }

    //Flushing method for Apex Logs. Ideally used when reprocessing
    global void flushProcessResultLog(){
        recordCount = 0;
        successCount = 0;
        errorCount = 0;
        failedIdSet = new Set<Id>();
        errorLogList = new List<Apex_Error_Log__c>();       
    }

    global virtual void handleAfterStopReprocessing(){
        //Do nothing, batch class can override this method if unique logic is applicable
    }

    //Method to reprocess failed records from the Batch process
    global void reprocessFailedRecords(){           
        try{
            Type batchClass = Type.forName(JobName);
            SCB_BatchMaster instance = (SCB_BatchMaster)batchClass.newInstance();
            instance.reprocessIdSet = failedIdSet;
            instance.reprocessCount = reprocessCount + 1;
            if(reprocessCount < batchConfig.Max_Reprocess_Count__c){
                Database.executeBatch(instance,(Integer)batchConfig.Batch_Size__c);
            }          
        } catch(Exception e){
            //Created Apex Error Log record
            createErrorLogInstance('', e.getTypeName(), e.getMessage(), 'reprocessFailedRecords');
        }
    }

    //Method that will initiate the next batch class
    global void callNextBatch(String clsName){
        try{
            Type batchClass = Type.forName(clsName);
            SCB_BatchMaster instance =  (SCB_BatchMaster)batchClass.newInstance();
            Database.executeBatch(instance,(Integer)batchConfig.Batch_Size__c);
        } catch(Exception e){
            //Created Apex Error Log record
            createErrorLogInstance('', e.getTypeName(), e.getMessage(), 'callNextBatch');
        }
    }

    //Method to log the finish in an email.
    global void logFinishToEmail(String jobId, Boolean needToFlush){
        String emailBody = batchConfig.Batch_Result_Email_Body_String__c;
        String failedIdsStr = (failedIdSet != null && failedIdSet.size() > 0) ? String.join(new List<Id>(failedIdSet), ',') : 'NONE';
        String subject = 'Summary when processing'+JobName;
        List<String> toAddresses = new List<String>();
        
        //Adds specific email addresses specified in the batch configuration
        if(String.isNotBlank(batchConfig.Batch_Result_Email_To_Address__c)) {
            List<String> emails = batchConfig.Batch_Result_Email_To_Address__c.split(',');
            toAddresses.addAll(emails);
        }
        
        //Includes running user to email receipient
        if(batchConfig.Send_Email_To_Running_User__c){
           User u = [SELECT Email FROM User WHERE Id = :UserInfo.getUserId()];
           toAddresses.add(u.Email);
        }
        
        //Updates email body content. Works like merge fields in email templates
        if(String.isNotBlank(emailBody)){
            if(emailBody.contains('{batchName}')){
                emailBody = emailBody.replace('{batchName}', JobName);
            }
            
            if(emailBody.contains('{recordCount}')){
                emailBody = emailBody.replace('{recordCount}', String.valueOf(recordCount));
            }
            
            if(emailBody.contains('{errorCount}')){
                emailBody = emailBody.replace('{errorCount}', String.valueOf(errorCount));
            }
            
            if(emailBody.contains('{successCount}')){
                emailBody = emailBody.replace('{successCount}', String.valueOf(successCount));
            }
            
            if(emailBody.contains('{failedSetString}')){
                emailBody = emailBody.replace('{failedSetString}', failedIdsStr);
            }
            
            if(emailBody.contains('{processLog}')){
                emailBody = emailBody.replace('{processLog}', processLog);
            }
            //Send email
            sendEmail(toAddresses, subject, emailBody);        
        }
        
        if(needToFlush){
            //COMMIT ERROR LOGS TO DATABASE
            commitErrorLog();
            flushProcessResultLog();
        }
    }

    //Method to log the finish in an email.
    global void logFinishToEmail(String className, Decimal successCount, Decimal errorCount,Decimal recordCount, Boolean needToFlush){   
        String record = String.valueOf(recordCount.format());
        String successRe = String.valueOf(successCount.format());
        String faildRe = String.valueOf(errorCount.format());
        String emailBody = batchConfig.Batch_Result_Email_Body_String__c;
        String failedIdsStr = (failedIdSet != null && failedIdSet.size() > 0) ? String.join(new List<Id>(failedIdSet), ',') : 'NONE';
        
        //Updates email body content. Works like merge fields in email templates
        if(emailBody.contains('{batchName}')){
            emailBody = emailBody.replace('{batchName}', className);
        }

        if(emailBody.contains('{recordCount}')){
            emailBody = emailBody.replace('{recordCount}', String.valueOf(recordCount));
        }

        if(emailBody.contains('{errorCount}')){
            emailBody = emailBody.replace('{errorCount}', String.valueOf(errorCount));
        }

        if(emailBody.contains('{successCount}')){
            emailBody = emailBody.replace('{successCount}', String.valueOf(successCount));
        }

        if(emailBody.contains('{failedSetString}')){
            emailBody = emailBody.replace('{failedSetString}', failedIdsStr);
        }

        if(emailBody.contains('{processLog}')){
            emailBody = emailBody.replace('{processLog}', processLog);
        }

        String subject = 'Summary when processing ' + className;
        //Adds specific email addresses specified in the batch configuration
        List<String> toAddresses = new List<String>{batchConfig.Batch_Result_Email_To_Address__c};
        //Send email
        sendEmail(toAddresses, subject, emailBody);
        
        if(needToFlush){
            //COMMIT ERROR LOGS TO DATABASE
            commitErrorLog();
            flushProcessResultLog();
        }
    }

    //Method to send email.
    global void sendEmail(List<String> emails, String subject, String body){
        sendEmail(emails, subject, body, null, true);
    }

    global void sendEmail(List<String> emails, String subject, String body, String targetObjectId, Boolean isSaveAsActivity){
        if(emails.size() > 0 && String.isNotBlank(body)){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject(subject);
            mail.setToAddresses(emails);
            mail.setUseSignature(false);
            mail.setSaveAsActivity(isSaveAsActivity);

            if(String.isNotBlank(targetObjectId)) {
                mail.setTargetObjectId(targetObjectId);
            }
            
            if(body.contains('</') || body.contains('/>')) {
                mail.setHtmlBody(body);
            } else {
                mail.setPlainTextBody(body);
            }

            if(!Test.isRunningTest()){
                Messaging.reserveSingleEmailCapacity(2);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            }   
        }
    }

    //Method creating an Apex Log
    global void createErrorLogInstance(String recordId, String exceptionType, String exceptionMessage, String methodName){
        errorLogList.add(SCB_ApexErrorLogUtility.createBatchLogInstance('', JobName, methodName, recordId, exceptionType, exceptionMessage, ''));
    }

    //Method to add apex log records processed count.
    global void addLogCount(Integer successCountToAdd, Integer failedCountToAdd, Integer totalProcessedCountToAdd){
        if(successCountToAdd > 0){
            successCount = successCount + successCountToAdd;
        }

        if(failedCountToAdd > 0){
            errorCount = errorCount + failedCountToAdd;
        }

        if(totalProcessedCountToAdd > 0){
            recordCount = recordCount + totalProcessedCountToAdd;
        }
    }

    //Method that will create the summary and execute saving of all apex logs to Apex Error Log Object
    global void logFinishToObject(Database.BatchableContext bc, Boolean needToFlush){
        //CREATE SUMMARY
        String failedIdsStr = (failedIdSet != null && failedIdSet.size() > 0) ? String.join(new List<Id>(failedIdSet), ',') : 'NONE';
        String batchExecuteSummary =  'Job Name: ' + JobName + '\r\n' +
                                    'Start Date Time: ' + String.valueof(startTime) + '\r\n' +
                                    'End Date Time: ' + String.valueof(DateTime.Now()) + '\r\n' +
                                    'Total Processed Records: ' + recordCount + '\r\n' +
                                    'Total Successful Records: ' + successCount + '\r\n' +
                                    'Total Failed Records: ' + errorCount + '\r\n';
        createErrorLogInstance('','Batch Summary',batchExecuteSummary,'');
        //COMMIT ERROR LOGS TO DATABASE
        commitErrorLog();
        if(needToFlush){
            flushProcessResultLog();
        }
    }

    //Method that will commit apex logs to database
    global void commitErrorLog(){
        if(errorLogList.size() > 0){
            SCB_ApexErrorLogUtility.logErrorList(errorLogList);
        }
    }

    //Method that will add conditions on a SOQL String
    global virtual String addDeltaCondition(String soqlStr, String deltaCondition){
        if(String.isNotBlank(deltaCondition)){
            if(soqlStr.contains(' WHERE ')){
                //SPLIT STRING 
                List<String> spltStr = soqlStr.split('WHERE');
                if(spltStr.size() > 0){
                    spltStr[0] +=  deltaCondition.startsWith('WHERE') ? deltaCondition : 'WHERE ' + deltaCondition;
                }
                soqlStr = String.join(spltStr, ' AND');
            } else {
                String objectTypeStr = soqlStr.substringAfterLast('FROM').trim().substringBefore(' ');
                List<String> spltStr = soqlStr.split(objectTypeStr);
                if(spltStr.size() > 0){
                    spltStr[0] += objectTypeStr + ' ' + ( deltaCondition.startsWith('WHERE') ? deltaCondition : 'WHERE ' + deltaCondition);
                }
                soqlStr = String.join(spltStr,' ');
            }
        }
        return soqlStr;
    }
    
    //Method that parse and replace merged fields in the delta condition
    global String replaceDeltaConditionMergeFields(String deltaCondition){
        if(String.isNotBlank(deltaCondition)){
            if(deltaCondition.contains('{startDate}')){
                deltaCondition = deltaCondition.replace('{startDate}', ' : startDate');
            }

            if(deltaCondition.contains('{endDate}')){
                deltaCondition = deltaCondition.replace('{endDate}', ' : endDate');
            }
        }
        return deltaCondition;
    }

    private String getBatchClassName(){
    	return String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
  	}
}