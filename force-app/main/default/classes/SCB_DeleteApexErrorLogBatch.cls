global class SCB_DeleteApexErrorLogBatch extends SCB_BatchMaster {
    
    global SCB_DeleteApexErrorLogBatch(){
        init('SCB_DeleteApexErrorLogBatch', 'Scheduled Delete Apex Error Log');
    }

    global override void execute(Database.BatchableContext context, List<sObject> objectList) {
        if(!objectList.isEmpty()){
            try {
                handlerDMLResult(Database.delete(objectList,false));
            } catch(Exception ex) {
                createErrorLogInstance('', ex.getTypeName(), ex.getMessage(), 'execute');
            }
        }
    }
}