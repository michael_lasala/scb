@isTest
private class SCB_DeleteApexErrorLogBatch_Test {
    @testSetup static void staticRecords(){
        
    }
    
    @isTest static void batch_Test(){
        List<Apex_Error_Log__c> aelList = new List<Apex_Error_Log__c>();
        for(Integer i = 0; i < 10; i++){
            Apex_Error_Log__c ael = new Apex_Error_Log__c(
                Trigger_Name__c = 'Trigger'+i, 
                Class_Name__c = 'Class'+i, 
                Method_Name__c = 'Method'+i
            );
            aelList.add(ael);
        }
        insert aelList;

        Test.StartTest();
        SCB_DeleteApexErrorLogBatch batchTest = new SCB_DeleteApexErrorLogBatch();
        batchTest.batchConfig.Delta_Load_Condition__c = 'CreatedDate = TODAY';
        Database.executeBatch(batchTest);
        Test.StopTest();
        
        //Only batch summary log remains
        System.assertEquals(1, [SELECT COUNT() FROM Apex_Error_Log__c]);
    }

    @isTest static void scheduler_Test(){
		String startTime = '0 0 12 * * ?';
        SCB_DeleteApexErrorLogScheduler testScheduler = new SCB_DeleteApexErrorLogScheduler();
        String jobId = System.schedule('Test job', '0 0 12 * * ?', testScheduler);
        
        CronTrigger c = [SELECT Id,
                                CronExpression,
                                TimesTriggered,
                                NextFireTime
                         FROM   CronTrigger
                         WHERE  Id =: jobId];
        
        System.assertEquals(startTime, c.CronExpression);
        System.assertEquals(0, c.TimesTriggered);
	}
}