@isTest
public class SCB_RelatedListController_Test {
    @isTest public static void getOpportunities(){
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(1);
        insert accountList;
        Opportunity opp = SCB_TestDataFactory.createOpportunity(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Onshore ETB').getRecordTypeId(), accountList.get(0).Id);
        opp.CloseDate = Date.Today()+1;
        opp.StageName = 'Not Attempted';
        insert opp;
                
        Map<String, Object> requestMap = new Map<String, Object>(); 
        requestMap.put(SCB_RelatedListController.FIELDS_PARAM, 'Name, StageName, Amount, CloseDate');
        requestMap.put(SCB_RelatedListController.RELATED_FIELD_API_NAME_PARAM, 'AccountId');
        requestMap.put(SCB_RelatedListController.RECORD_ID_PARAM, accountList.get(0).Id);
        requestMap.put(SCB_RelatedListController.NUMBER_OF_RECORDS_PARAM, 1);
        requestMap.put(SCB_RelatedListController.SOBJECT_API_NAME_PARAM, 'Opportunity');
        requestMap.put(SCB_RelatedListController.SORTED_BY_PARAM, 'Name');
        requestMap.put(SCB_RelatedListController.SORTED_DIRECTION_PARAM, 'ASC');
        requestMap.put(SCB_RelatedListController.OFFSET_OF_RECORDS_PARAM, 0);
        requestMap.put(SCB_RelatedListController.CHECK_API_VISIBILITY, 'Id');
                
        String jsonData = SCB_RelatedListController.initData(JSON.serialize(requestMap));
        Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(jsonData);
        List<Object> records = (List<Object>)responseMap.get(SCB_RelatedListController.RECORDS_PARAM);
        System.assert(!records.isEmpty());
        String iconName = (String)responseMap.get(SCB_RelatedListController.ICON_NAME_PARAM);
        System.assert(String.isNotBlank(iconName));
    }
}