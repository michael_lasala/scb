global class SCB_DeleteApexErrorLogScheduler implements Schedulable {

    global void execute(SchedulableContext sc){
        Batch_Configuration__mdt config = [SELECT Batch_Size__c FROM Batch_Configuration__mdt WHERE DeveloperName = 'SCB_DeleteApexErrorLogBatch' LIMIT 1];
        Database.executeBatch(new SCB_DeleteApexErrorLogBatch(), Integer.valueof(config.Batch_Size__c));
    }
}