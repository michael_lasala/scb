import { LightningElement,track,wire,api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import { NavigationMixin } from 'lightning/navigation';
import getSegments from '@salesforce/apex/SCB_ClientAccountDetailsController.getSegments';
import getFinancialAccounts from '@salesforce/apex/SCB_ClientAccountDetailsController.getFinancialAccounts';
import labelNoRecord from '@salesforce/label/c.SCB_No_Record';
import labelSegmentAccountType from '@salesforce/label/c.SCB_Segment_Account_Type';

export default class ScbClientAccountDetails extends NavigationMixin( LightningElement ) {
    @api recordId = '';
    
    @track gridColumns = [
        {
            type: 'text',
            fieldName: 'accountType',
            label: 'Account Type', 
        },
        {
            type: 'currency',
            fieldName: 'balance',
            label: 'Balance',
        },
        {
            type: 'date',
            fieldName: 'maturityDate',
            label: '3M Sales/Maturity Date',
        },
        {
            type: "action",
            typeAttributes: {
                rowActions: [{label: 'View', name: 'view'}]
            }
        }
    ];

    @track gridData;
    @track expandedRows;
    @track gridLoadingState = false;
    childrenData = {};
    excludeRecordIds = [];

    @wire (getSegments, {accountId: '$recordId'}) accounts({error,data}){
        if(data){
            var tempData = [];
            var tempRows = [];
            for(var key in data){
                for(var innerKey in data[key]){
                    var tempType = new Object();
                    tempType.id = key
                    tempType.accountType = key;
                    tempType.balance = innerKey;
                    tempType.maturityDate = null;
                    var tempChildData = [];
                    for(var i = 0; i < data[key][innerKey].length; i++){
                        // console.log('$: '+JSON.stringify(data[key][innerKey][i]));
                        var accType = {};
                        accType.id = data[key][innerKey][i].recordId;
                        accType.accountType = data[key][innerKey][i].accountType;
                        accType.balance = data[key][innerKey][i].balance;
                        accType.maturityDate = null;
                        if(data[key][innerKey][i].balance > 0){
                            accType._children = [];
                        }
                        tempChildData.push(accType);
                        this.excludeRecordIds.push(data[key][innerKey][i].recordId);
                    }
                    tempType._children = tempChildData;
                    tempData.push(tempType);
                    // tempRows.push(key);
                    this.excludeRecordIds.push(key);
                }
            }
            this.gridData = tempData;
            this.expandedRows = tempRows;
            this.error=undefined;
        } else if(error){
            this.error = error;
            this.accounts = undefined;
        }
    }
    

    handleRowToggle(event) {
        const rowName = event.detail.name;
        const hasChildrenContent = event.detail.hasChildrenContent;
        const row = event.detail.row;
        
        if (hasChildrenContent === false) {
            this.gridLoadingState = true;
            getFinancialAccounts({accountId: this.recordId, recordTypeName: row.accountType})
            .then(result => {
                // console.log('$ success: '+JSON.stringify(result));
                this.childrenData = result;
                // console.log('$$ :'+JSON.stringify(this.childrenData[rowName]));
                this.retrieveUpdatedData(rowName).then(newData => {
                    this.gridData = newData;
                    this.gridLoadingState = false;
                });
            })
            .catch(error => {
                // console.log('$ error: '+JSON.stringify(error));
                if (Array.isArray(error.body)) {
                    this.error = error.body.map(e => e.message).join(', ');
                } else if (typeof error.body.message === 'string') {
                    this.error = error.body.message;
                }
                // console.log('$ this.error: '+this.error);

                const event = new ShowToastEvent({
                    title: labelError,
                    message: this.error,
                    variant: 'error', 
                    mode: 'sticky',
                });
                this.dispatchEvent(event);
            });
        }
    }

    retrieveUpdatedData(rowName) {
        return new Promise(resolve => {
            const updatedData = this.addChildrenToRow(
                this.gridData,
                rowName,
                this.childrenData[rowName]
            );
            resolve(updatedData);
        });
    }

    addChildrenToRow(data, rowName, children) {
        const newData = data.map(row => {
            let hasChildrenContent = false;

            if(row.hasOwnProperty('_children') &&
                Array.isArray(row._children) &&
                row._children.length > 0){
                hasChildrenContent = true;
            }

            if(row.id === rowName){
                row._children = children;
            } else if(hasChildrenContent){
                this.addChildrenToRow(row._children, rowName, children);
            }
            return row;
        });
        return newData;
    }

    handleRowActions(event) {
        let actionName = event.detail.action.name;
        let row = event.detail.row;
        // console.log('$ actionName: '+actionName);
        // console.log('$ row: '+JSON.stringify(row));
        
        if(this.excludeRecordIds.includes(row.id)){
            const event = new ShowToastEvent({
                title: labelNoRecord,
                message: labelSegmentAccountType,
            });
            this.dispatchEvent(event);
        } else {
            switch (actionName) {
                case 'view':
                    this[NavigationMixin.GenerateUrl]({
                        type: 'standard__recordPage',
                        attributes: {
                            recordId: row.recordId,
                            actionName: 'view',
                        },
                    })
                    .then(url => {
                        window.open(url, "_blank");
                    });
                    break;
            }
        }
    }
}