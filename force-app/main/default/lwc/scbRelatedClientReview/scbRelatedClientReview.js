import { LightningElement, api, track } from 'lwc';

export default class ScbRelatedClientReview extends LightningElement {

    @api recordId;
    defaultValues;

    connectedCallback(){ 
        this.defaultValues = {Client_Name__c: this.recordId}
    }
    
    renderedCallback(){
        this.accountId = this.recordId
    }

    @track clientReviewColumns = [
        {
            label: 'Client Review Ref. No',
            fieldName: 'Name',
            type: 'customurl',
            typeAttributes: {
                urlLink: {
                    fieldName : 'LinkName'
                },
                isAccessible: {
                    fieldName : 'Id_isVisible'
                },
                objectName: 'Client_Review__c',
                recordId: {
                    fieldName : 'Id'
                }
                // ,
                // recordType: {
                //     fieldName : 'RecordType_DeveloperName'
                // }
            }
        },
        { label: 'Assessment Date', fieldName: 'Assessment_Date__c', type: 'date-local', typeAttributes:{month:"2-digit", day:"2-digit"} },
        { label: 'Last Review Date', fieldName: 'Last_Review_Date__c', type:"date-local", typeAttributes:{month:"2-digit", day:"2-digit"} }
    ]

    accountId
    customActions = [{ label: 'Custom action', name: 'custom_action' }]
}