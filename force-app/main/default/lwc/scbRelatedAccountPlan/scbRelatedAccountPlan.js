import { LightningElement, api, track } from 'lwc';

export default class ScbRelatedAccountPlan extends LightningElement {
    @api recordId;
    defaultValues;

    connectedCallback(){ 
        this.defaultValues = {Client_Name__c: this.recordId}
    }

    @track accPlanColumns = [
        { 
            label: 'Account Plan Ref No.', 
            fieldName: 'Name', 
            type: 'customurl', 
            typeAttributes: {
                urlLink: {
                    fieldName : 'LinkName'
                },
                isAccessible: {
                    fieldName : 'Id_isVisible'
                },
                restrictAccess: false,
                objectName: 'Account_Plan__c',
                recordId: {
                    fieldName : 'Id'
                }
            } 
        },
        { label: 'Overall Status', fieldName: 'Overall_Status__c', type:"text"},
        { label: 'Account Plan Reviewer', fieldName: 'Account_Plan_Reviewer__r_Name', type:"text"},
        { label: 'Review Date', fieldName: 'Review_Date__c', type:"date-local", typeAttributes:{month:"2-digit", day:"2-digit"} }
    ]
    accountId;
    customActions = [{ label: 'Custom action', name: 'custom_action' }];
    renderedCallback(){
        this.accountId = this.recordId
    }
}