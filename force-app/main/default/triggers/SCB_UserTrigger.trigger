trigger SCB_UserTrigger on User (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	if(SCB_QueryWithoutSharing.getTriggerControl('SCB_UserTrigger').Active__c){
	    SCB_UserTriggerClass.executeTriggerEvents(trigger.isBefore, trigger.isAfter, 
	                                              trigger.isInsert, trigger.isUpdate,trigger.isDelete, trigger.isUndelete, 
	                                              trigger.oldMap, trigger.new, trigger.newMap);
	}
}