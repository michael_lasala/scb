#!groovy

node {

    def SF_CONSUMER_KEY=env.SF_CONSUMER_KEY
    def SF_USERNAME=env.SF_USERNAME
    def SERVER_KEY_CREDENTIALS_ID=env.SERVER_KEY_CREDENTIALS_ID
    def DEPLOY_METADATA=env.DEPLOY_METADATA
    def SF_INSTANCE_URL = env.SF_INSTANCE_URL ?: "https://login.salesforce.com"
    def toolbelt = tool 'toolbelt'

    // -------------------------------------------------------------------------
    // Check out code from source control.
    // -------------------------------------------------------------------------
    stage('checkout source') {
        checkout scm
    }

    // -------------------------------------------------------------------------
    // Run all the enclosed stages with access to the Salesforce
    // JWT key credentials.
    // -------------------------------------------------------------------------
 	withEnv(["HOME=${env.WORKSPACE}"]) {	
	
	    withCredentials([file(credentialsId: SERVER_KEY_CREDENTIALS_ID, variable: 'server_key_file')]) {
            
		
			// -------------------------------------------------------------------------
			// Authenticate to Salesforce using the server key.
			// -------------------------------------------------------------------------
			stage('Authorize to Salesforce') {
				rc = command "${toolbelt}/sfdx force:auth:jwt:grant --instanceurl ${SF_INSTANCE_URL} --clientid ${SF_CONSUMER_KEY} --jwtkeyfile ${server_key_file} --username ${SF_USERNAME} --setalias SPRINTST"
				if (rc != 0) {
			    	error 'Salesforce org authorization failed.'
				}
			}
            
            // -------------------------------------------------------------------------
			// Install Salesforce Git Delta
			// -------------------------------------------------------------------------
			stage('Install Salesforce Git Delta') {
				command "echo 'y' | ${toolbelt}/sfdx update"
				command "echo 'y' | ${toolbelt}/sfdx plugins:install sfdx-git-delta"
			}
            		
			// -------------------------------------------------------------------------
			// Get Repository Changes
			// -------------------------------------------------------------------------
			stage('Get Delta') {
				command "${toolbelt}/sfdx sgd:source:delta -f HEAD -t HEAD^ --output ."
				command "cat package/package.xml" // show what was the changes
			}
            
			// -------------------------------------------------------------------------
			// Deploy and run tests
			// -------------------------------------------------------------------------
			stage('Deploy and Run Tests') {
				if(env.DEPLOY_METADATA == "1"){
					command "echo Running Deployment"
					rc = command "${toolbelt}/sfdx force:source:deploy -u ${SF_USERNAME} -x package/package.xml -l RunLocalTests"
				} 
				else {
					command "echo Running Validation"
					rc = command "${toolbelt}/sfdx force:source:deploy -u ${SF_USERNAME} -x package/package.xml -l RunLocalTests -c"
				}

				if (rc != 0) {
				    error 'Salesforce deploy and test run failed.'
				}
				else {
					command "cat destructiveChanges/destructiveChanges.xml" // show what is to destroy

					if(env.DEPLOY_METADATA == "1"){
						command "${toolbelt}/sfdx force:mdapi:deploy -u ${SF_USERNAME} -d destructiveChanges -w 10000"
					}
					else {
						command "${toolbelt}/sfdx force:mdapi:deploy -u ${SF_USERNAME} -d destructiveChanges -w 10000 -c"
					}
				}
			}
	    }
	}
}

def command(script) {
    if (isUnix()) {
        return sh(returnStatus: true, script: script);
    } else {
		return bat(returnStatus: true, script: script);
    }
}